#include <stdio.h>
#include <math.h>

int main()
{
	float a,b,c,s,p,taxRate;
	printf("Enter the side of your site >> ");
	if(scanf("%f %f %f",&a,&b,&c)==0)
	{
		printf("ERROR!!! INVALID DATA ENTRY!!!");
		getchar();
		getchar();
		getchar();
		return 1;
	}
    if (a<0 || b<0 || c<0)
	{
		printf("ERROR!!! PARTY DOWN 0!!! CALCULATION IMPOSSIBLE!!!");
		getchar();
		getchar();
		return 2;
	}
	printf("Enter the rate of tax in the form of rubles / sq.m. >> ");
	if(scanf("%f",&taxRate)==0)
	{
		printf("ERROR!!! INVALID DATA ENTRY!!!");
		getchar();
		getchar();
		getchar();
		return 1;
	}
	if (taxRate<0)
	{
		printf("ERROR!!! TAX RATE LESS 0!!! CALCULATION IMPOSSIBLE!!!");
		getchar();
		getchar();
		return 2;
	}
	p=(a+b+c)/2;
	s=sqrt(p*(p-a)*(p-b)*(p-c));
	printf("Tax section of triangular shape with sides %f, %f � %f, with the tax rate %f rubles / sq.m. is >> %f",a,b,c,taxRate,s*taxRate);
	getchar();
	getchar();
	return 0;
}
		